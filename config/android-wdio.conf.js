exports.config = {
    port: 4723,
    runner: 'local',
    specs: [
        '../test/specs/android/*.js'
    ],
    exclude: [
    ],
    maxInstances: 1,
    capabilities: [{
        platformName: 'Android', 
        "appium:device-name": 'Pixel_3a_API_30',
        "appium:platformVersion": "11",
        "appium:automationName": "UIAutomator2",
        "appium:app": "/Users/napatrtansutiraphong/Downloads/app.apk"
    }],
    logLevel: 'info',
    bail: 0,
    baseUrl: 'http://localhost',
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: ["appium"],
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
        ui: 'bdd',
        timeout: 6000000
    }
}
