exports.config = {
    port: 4723,
    runner: 'local',
    specs: [
        '../test/specs/ios/*.js'
    ],
    exclude: [
    ],
    maxInstances: 1,
    capabilities: [{
        platformName: 'IOS', 
        "appium:device-name": 'iPhone 14 Pro',
        "appium:platformVersion": "16.2",
        "appium:automationName": "XCUItest",
        "appium:app": "/Users/napatrtansutiraphong/Downloads/app.app"
    }],
    logLevel: 'info', // Level of logging verbosity: trace | debug | info | warn | error | silent
    bail: 0,
    baseUrl: 'http://localhost',
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    services: ["appium"],
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
        ui: 'bdd',
        timeout: 6000000
    }
}
