const commonFunction = require("../../screenObjects/commonFunction.js")
const login = require("../../screenObjects/android/Login.screen.js")
const landing = require("../../screenObjects/android/Landing.screen.js")
const credit = require("../../screenObjects/android/Credit.screen.js")

describe("Demo SCB Appium", () => {

  it("Test Login FastEasy", async () => {
    await login.LoginToLanding("nano_1004")
    await landing.iClickToCreditScrren()
    await credit.iClickToUpScreen()
    await driver.pause(5000);
    await credit.iClickHowTo()
    await driver.pause(5000);
  });
});
