const commonFunction = require("../../screenObjects/commonFunction.js")
const login = require("../../screenObjects/ios/Login.screen.js")
const landing = require("../../screenObjects/ios/Landing.screen.js")
const credit = require("../../screenObjects/ios/Credit.screen.js")

describe("Demo SCB Appium", () => {

  it("Test Login FastEasy", async () => {
    await driver.pause(10000);
    await login.LoginToLanding("nano_1004")
    await landing.iClickToCreditScrren()
    await credit.iClickToUpScreen()
    // await credit.iClickApply()
    await driver.pause(5000);
    await credit.iClickHowTo()
    await driver.pause(5000);
  });
});
