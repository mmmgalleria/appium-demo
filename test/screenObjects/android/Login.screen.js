const commonFunction = require('../commonFunction.js')

class LoginScreen {
  async LoginToLanding(deviceID) {
    await $('id=com.scb.phone_sit_cr:id/device_id').setValue(deviceID);
    await commonFunction.waitForElementAndClick('id=com.scb.phone_sit_cr:id/default_button_text_view', 1)
    await commonFunction.waitForElementAndClick('id=com.scb.phone_sit_cr:id/skip_tutorial_image_view')
    await commonFunction.waitForElementAndClick('id=com.scb.phone_sit_cr:id/skip_lifestyle_tutorial_image_view', 1)
    await commonFunction.waitForElementAndClick('id=com.android.permissioncontroller:id/permission_allow_foreground_only_button', 1)
    await commonFunction.waitForElementAndClick('id=com.scb.phone_sit_cr:id/iv_close_ads', 1)
    await expect($('id=com.scb.phone_sit_cr:id/iv_avatar')).toBeDisplayed();
  }
}

module.exports = new LoginScreen();
