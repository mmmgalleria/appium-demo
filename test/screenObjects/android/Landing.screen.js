const commonFunction = require('../commonFunction.js')

class LandingScreen {
  async iClickToCreditScrren() {
    await commonFunction.scrollDown(driver)
    const selector = 'new UiSelector().text("Apply Loan & Credit Card\nสมัครสินเชื่อ และบัตรเครดิต").className("android.widget.TextView")'
    const creditBanner = await $(`android=${selector}`)
    await creditBanner.click()
    await commonFunction.iEnterPin()
    await expect($('id=com.scb.phone_sit_cr:id/title_text_view')).toBeDisplayed();
  }
}

module.exports = new LandingScreen();
