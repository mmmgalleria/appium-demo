const commonFunction = require('../commonFunction.js')

class CreditScreen {
  async iClickToUpScreen() {
    const selector = 'new UiSelector().text("UP Ngern Tunjai").className("android.widget.TextView")'
    const upBanner = await $(`android=${selector}`)
    await upBanner.click()
    await commonFunction.waitForElement('~สินเชื่อ UP by SCB')
    await expect($('~สินเชื่อ UP by SCB')).toBeDisplayed();
  }

  async iClickApply() {
    await commonFunction.waitForElementAndClick('~สมัครเลย')
    await commonFunction.waitForElementAndClick('~สมัครภายหลัง')
  }

  async iClickHowTo() {
    await commonFunction.waitForElement('~สินเชื่อ UP by SCB')
    await commonFunction.scrollDown(driver)
    await commonFunction.waitForElementAndClick('~วิธีการสมัคร')
    await expect($('~สมัคร UP ยังไง')).toBeDisplayed();
  }
}

module.exports = new CreditScreen();
