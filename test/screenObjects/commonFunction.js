class CommonFunction {

    async waitForElement(targetElement, timeout = 5) {
        for (let i = 0; i < timeout; i++) {
            if(await $(targetElement).isDisplayed()) {
                return true
            }
            await driver.pause(1000);
        }
        return false
    }

    async waitForElementAndClick(targetElement, timeout = 5) {
        if (await this.waitForElement(targetElement, timeout)) {
            await $(targetElement).click();
        }
    }

    async iEnterPin() {
        if (driver.isAndroid) {
            if (await this.waitForElement('id=com.scb.phone_sit_cr:id/button_1', 3)) {
                await $('id=com.scb.phone_sit_cr:id/button_1').click();
                await $('id=com.scb.phone_sit_cr:id/button_1').click();
                await $('id=com.scb.phone_sit_cr:id/button_2').click();
                await $('id=com.scb.phone_sit_cr:id/button_2').click();
                await $('id=com.scb.phone_sit_cr:id/button_3').click();
                await $('id=com.scb.phone_sit_cr:id/button_3').click();
            }
        } else {
            if (await this.waitForElement('id=btn_key_1', 3)) {
                await $('id=btn_key_1').click();
                await $('id=btn_key_1').click();
                await $('id=btn_key_2').click();
                await $('id=btn_key_2').click();
                await $('id=btn_key_3').click();
                await $('id=btn_key_3').click();
            }
        }
    }

    async scrollDown(driver, scrollDuration = 300) {
        const startPercentage = 70
        const endPercentage = 10
        const anchorPercentage = 50
    
        const { width, height } = await driver.getWindowSize()
        if (driver.isAndroid) {
            var density = (await driver.getDisplayDensity()) / 100
        } else {
            var density = 5
        }
        const anchor = (width * anchorPercentage) / 100
        const startPoint = (height * startPercentage) / 100
        const endPoint = (height * endPercentage) / 100
    
        await driver.performActions([
            {
                type: 'pointer',
                id: 'finger1',
                parameters: { pointerType: 'touch' },
                actions: [
                    { type: 'pointerMove', duration: 0, x: anchor, y: startPoint },
                    { type: 'pointerDown', button: 0 },
                    { type: 'pause', duration: 100 },
                    { type: 'pointerMove', duration: scrollDuration, origin: 'pointer', x: 0, y: -endPoint * density },
                    { type: 'pointerUp', button: 0 },
                    { type: 'pause', duration: scrollDuration },
                ],
            },
        ])
    }
}
  
module.exports = new CommonFunction();