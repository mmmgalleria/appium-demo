const commonFunction = require('../commonFunction.js')

class LandingScreen {
  async iClickToCreditScrren() {
    await commonFunction.scrollDown(driver)
    const selector = `label == "Apply Loan & Credit Card"`
    const creditBanner = await $(`-ios predicate string:${selector}`)
    await creditBanner.click()
    await commonFunction.iEnterPin()
    await expect($('id=txt_navigation_bar_title')).toBeDisplayed();
  }
}

module.exports = new LandingScreen();
