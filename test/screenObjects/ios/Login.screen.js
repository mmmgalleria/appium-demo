const commonFunction = require('../commonFunction.js')

class LoginScreen {
  async LoginToLanding(deviceID) {
    await $('id=edit_log_in_device_id').setValue("nano_1004")
    await commonFunction.waitForElementAndClick('id=btn_ok_log_in')
    await commonFunction.waitForElementAndClick('id=btn_close')
    await commonFunction.waitForElementAndClick('id=btn_life_style_landing_close')
    await commonFunction.waitForElementAndClick('id=Allow')
    await commonFunction.waitForElementAndClick('id=Allow While Using App')
    await expect($('id=img_header_profile_image').isDisplayed());
  }
}

module.exports = new LoginScreen();
